﻿<%@ Assembly Name="Microsoft.SharePoint.ApplicationPages, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Assembly Name="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Assembly Name="Prexens.Hoozin.Engine, Version=14.0.0.0, Culture=neutral, PublicKeyToken=0afb724dcb92ffcb" %>

<%@ Page Language="C#" Inherits="Microsoft.SharePoint.ApplicationPages.ErrorPage" MasterPageFile="/_layouts/15/Prexens/hoozin/MasterPages/login.master" %>

<%@ Import Namespace="Microsoft.SharePoint.WebControls" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="wssuc" TagName="FoldHyperLink" src="~/_controltemplates/15/FoldHyperLink.ascx" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<asp:Content ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    <SharePoint:EncodedLiteral runat="server" EncodeMethod="HtmlEncode" Text="<%$PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_Hoozin500_PageTitle%>" ID="ClaimsFormsPageTitle" />
</asp:Content>
<asp:Content ContentPlaceHolderId="PlaceHolderPageTitleInTitleArea" runat="server">
	<asp:Panel id="ErrorPageTitlePanel" runat="server">
	</asp:Panel>
</asp:Content>
<asp:Content ID="Content4" contentplaceholderid="PlaceHolderAdditionalPageHead" runat="server">
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
	<link href="/_layouts/15/EucouncilRed/Hoozin/LESS/hoozinBoxPage.less?v=1002" rel="stylesheet" type="text/css"  />

    <script type="text/javascript">
		var goBackOrGoHome = function () {
			if (typeof history === 'object' && history.length > 1) {
				history.back();
			}
			else {
				window.location.href = '<%= SPContext.Current.Site.Url %>';
			}
		}
	</script>

</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceHolderSiteName" runat="server" />

<asp:Content ContentPlaceHolderID="PlaceHolderMain" runat="server">
	<div id="boxScreenWrapper" class="errors crash">
		<!-- Zone For Informations or Picture -->
		<div id="infosZone">
			<span>500</span>
		</div>

		<!-- Principale Zone -->
		<div class="contentZone">

			<div id="hoozinLogo"></div>

			<h3>
				<SharePoint:EncodedLiteral ID="EncodedLiteral8" runat="server" Text="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_Hoozin500_PageHeaderText %>" />
			</h3>
			
			<p class="errorMessage">
				<SharePoint:EncodedLiteral ID="EncodedLiteral5" runat="server" Text="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_Hoozin500_ErrorMessagePart1 %>" /> 
				<a href="javascript:history.back()">
					<SharePoint:EncodedLiteral ID="EncodedLiteral6" runat="server" Text="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_Hoozin500_ErrorMessagePart2 %>" /> 
				</a>.
			</p>
			
			<p class="errorCode">
				<SharePoint:EncodedLiteral ID="EncodedLiteral7" runat="server" Text="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_Hoozin500_ErrorCode %>" /> 
				<a id="knowMoreOnError" href="#" onclick="javascript:openError();">
					<SharePoint:EncodedLiteral ID="EncodedLiteral9" runat="server" Text="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_Hoozin500_KnowMore %>" /> 
				</a>
			</p>
						
			<p id="sharepointError" style="display: none;">
				<SharePoint:FormattedString ID="LabelMessage" EncodeMethod="HtmlEncodeAllowSimpleTextFormatting" runat="server">
					<asp:HyperLink ID="LinkContainedInMessage" runat="server" />
				</SharePoint:FormattedString>
				<br />

				<asp:HyperLink ID="AdditionalHelpLink" Visible="false" runat="server" />

				<!--
				<asp:Panel id="FoldPanel" class="ms-error-detailsFold" runat="server">
					<wssuc:FoldHyperLink id="FoldLink" runat="server" LinkTitleWhenFoldOpened="<%$Resources:wss,error_pagetechieDetails%>" >
					
						<asp:Panel id="WSSCentralAdmin_TroubleshootPanel" runat="server">
							<SharePoint:FormattedString id="helptopic_WSSCentralAdmin_Troubleshoot" FormatText="<%$Resources:wss,helptopic_link%>" EncodeMethod="NoEncode" runat="server"> 
							    <SharePoint:EncodedLiteral runat="server" text="<%$Resources:wss,troubleshoot_issues%>" EncodeMethod='HtmlEncode'/> 
                                <SharePoint:EncodedLiteral runat="server" text='WSSCentralAdmin_Troubleshoot' EncodeMethod='EcmaScriptStringLiteralEncode'/> 
                            </SharePoint:FormattedString>
						</asp:Panel>
						<asp:Panel id="WSSEndUser_troubleshootingPanel2" runat="server">
							<SharePoint:FormattedString id="helptopic_WSSEndUser_troubleshooting" FormatText="<%$Resources:wss,helptopic_link%>" EncodeMethod="NoEncode" runat="server"> 
                                <SharePoint:EncodedLiteral runat="server" text="<%$Resources:wss,troubleshoot_issues%>" EncodeMethod='HtmlEncode'/> 
                                <SharePoint:EncodedLiteral runat="server" text='WSSEndUser_troubleshooting' EncodeMethod='EcmaScriptStringLiteralEncode'/> 
                            </SharePoint:FormattedString>
						</asp:Panel>
					
				</wssuc:FoldHyperLink>
				
				</asp:Panel>-->

				<asp:Label ID="RequestGuidText" runat="server" />
				<br/>
				<asp:Label ID="DateTimeText" runat="server" />

				<script type="text/javascript" language="JavaScript">
				// <![CDATA[
					function ULSvam() { var o = new Object; o.ULSTeamName = "Microsoft SharePoint Foundation"; o.ULSFileName = "error.aspx"; return o; }
					var gearPage = document.getElementById('GearPage');
					if (null != gearPage) {
						gearPage.parentNode.removeChild(gearPage);
						document.title = "<SharePoint:EncodedLiteral runat='server' text='<%$Resources:wss,error_pagetitle%>' EncodeMethod='HtmlEncode'/>";
					}
					function _spBodyOnLoad() {
						ULSvam: ;
						var intialFocus = document.getElementById("errorPageTitleSpan");
						try {
							intialFocus.focus();
						}
						catch (ex) {
						}
					}
				// ]]>
				</script>
			</p><!--sharepointError-->
						
			<!-- Return button -->
			<a href="javascript:goBackOrGoHome();" class="btn btn-primary"><asp:Label ID="Label1" runat="server" Text="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_Hoozin401_Path %>"></asp:Label></a>
		</div>

	</div>
	
	<script type="text/javascript">
		
		function openError () {
			var errorDiv = document.getElementById('sharepointError');
			errorDiv.style.display = 'block';
		}

	</script>

</asp:Content>
