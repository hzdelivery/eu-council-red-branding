﻿<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:pxs="http://xml.prexens.com/" xmlns:rs="urn:schemas-microsoft-com:rowset" xmlns:z="#RowsetSchema">

  <xsl:output indent="no" method="html"/>

  <!-- Parameters -->
  <xsl:param name="hoozin-form-url" />
  <xsl:param name="previous-notifications" />

  <!-- Includes list -->
  <xsl:include href="Prexens.Hoozin.SharePoint.Xml.IncludesTemplates, Prexens.Hoozin.SharePoint, Version=2.0.0.0, Culture=neutral, PublicKeyToken=c6306360fc636a22" />
  <xsl:include href="/_layouts/EucouncilRed/Hoozin/XSLT/Mailing/includes.xslt" />

  <xsl:key name="notif" match="Notification" use="PostID" />
  <xsl:key name="action-plugin-id" match="Notification" use="ActionPluginID" />
  <xsl:key name="author-sid" match="Notification" use="AuthorSid" />

  <!-- Root of rendering logic -->
  <xsl:template match="/Hoozin">

    <xsl:variable name="notifications" select="Notifications/Notification" />
    <xsl:variable name="current-profile" select="$notifications[1]/OwnerSid" />
    <xsl:variable name="reactions-count" select="count($notifications[Type = 'Action' and Source = 'My' and DiscussionID = ''])" />
    <xsl:variable name="discussion-reactions" select="count($notifications[Type = 'Action' and Source = 'My' and DiscussionID != ''])" />
    <xsl:variable name="discussions-count" select="count($notifications[Type = 'Post' and Source = 'My' and DiscussionID != ''])" />
    <xsl:variable name="posts-count" select="count($notifications[Type = 'Post' and Source = 'My' and DiscussionID = ''])" />
    <xsl:variable name="profiles" select="Profiles/Profile" />
    <xsl:variable name="communities" select="Communities/Community" />

    <html>
      <head>
        <title>Email</title>

        <xsl:call-template name="css-email">
          <xsl:with-param name="mail-type" select="'Notifications'" />
        </xsl:call-template>

      </head>
      <body>

        <table align="center" width="550" border="0" cellspacing="0" cellpadding="0" class="hz-email">
          <tr>
            <td bgcolor="{$background-color}" height="60">

              <xsl:call-template name="header-email">
                <xsl:with-param name="mail-type" select="'Notifications'" />
              </xsl:call-template>

            </td>
          </tr>

          <tr>
            <td bgcolor="{$background-color}">

              <table border="0" cellspacing="0" cellpadding="0" width="550">
                <tr>
                  <td>

                    <table  border="0" cellspacing="0" cellpadding="0" width="550" style="margin-left:18px; color:{$text-color}; font-family:'Tahoma', Tahoma;">
                      <tr>
                        <td>

                          <table style="width:100%;" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td valign="middle" height="40" style="font-family:'Tahoma', Tahoma; font-size:11px; color:{$footer-text-color};">
                                <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_SummaryTitle')" />
                                <xsl:text>&#160;</xsl:text>
                                <xsl:value-of select="pxs:GetLocalDateFromString($previous-notifications, 'g')" />
                              </td>
                            </tr>
                          </table>

                          <table width="550" border="0" cellspacing="0" cellpadding="0">

                            <tr>
                              <td style="background-color:{$background-color}; border-bottom:1px solid {$border-top-color};">

                                <xsl:if test="count($notifications[Source = 'My']) &gt; 0">

                                  <table style="color:{$text-color};" border="0" cellspacing="0" cellpadding="5" width="100%">
                                    <tr>
                                      <td height="30" width="30" style="font-family:'Tahoma', Tahoma;">
                                        <img style="vertical-align:middle;" src="cid:[contentId:notificationmy,TEMPLATE\images\EucouncilRed\Hoozin\Mailing\notif-my-icon.png]" />
                                      </td>
                                      <td height="30" style="font-family:'Tahoma', Tahoma;">
                                        <span style="color: {$text-color}; font-size: 13px; font-weight: bold; font-family:'Tahoma', Tahoma;">
                                          <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_My')" />
                                        </span>
                                      </td>
                                    </tr>
                                  </table>


                                  <table width="100%" cellspacing="0" border="0" style="border-top:1px solid {$background-light-color};">
                                    <xsl:if test="$reactions-count > 0" >
                                      <tr>
                                        <td width="25%" height="70" valign="middle" align="center">
                                          <a href="{$hoozin-form-url}/my.aspx">
                                            <img src="cid:[contentId:notificationlikelogo,TEMPLATE\images\EucouncilRed\Hoozin\Mailing\my-notif-email-like.png]" style="border:none;"/>
                                          </a>
                                        </td>
                                        <td width="75%">

                                          <table cellspacing="0" border="0" width="100%" style="font-family:'Tahoma', Tahoma;">
                                            <tr>
                                              <td valign="middle">
                                                <a href="{$hoozin-form-url}/my.aspx" style="font-family:'Tahoma', Tahoma;font-size:35px; color:{$link-color}; text-decoration:none;">
                                                  <xsl:value-of select="$reactions-count"/>
                                                </a>
                                              </td>
                                              <td style="font-size:14px;color:{$text-color};" valign="middle">
                                                <xsl:choose>
                                                  <xsl:when test="$reactions-count > 1">
                                                    <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_ReactInYourContentPlurial')" />
                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                    <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_ReactInYourContent')" />
                                                  </xsl:otherwise>
                                                </xsl:choose>
                                              </td>
                                            </tr>
                                          </table>

                                        </td>
                                      </tr>
                                    </xsl:if>

                                    <xsl:if test="$discussion-reactions > 0" >
                                      <tr>
                                        <td width="25%" height="70" valign="middle" align="center">
                                          <a href="{$hoozin-form-url}/my.aspx">
                                            <img src="cid:[contentId:notificationlikelogo,TEMPLATE\images\EucouncilRed\Hoozin\Mailing\my-notif-email-like.png]" />
                                          </a>
                                        </td>
                                        <td width="75%">

                                          <table cellspacing="0" border="0" width="100%" style="font-family:'Tahoma', Tahoma;">
                                            <tr>
                                              <td valign="middle">
                                                <a href="{$hoozin-form-url}/my.aspx" style="font-family:'Tahoma', Tahoma;font-size:35px; color:{$link-color}; text-decoration:none;">
                                                  <xsl:value-of select="$discussion-reactions"/>
                                                </a>
                                              </td>
                                              <td valign="middle">
                                                <xsl:choose>
                                                  <xsl:when test="$discussion-reactions > 1">
                                                    <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_ReactInYourDiscussionsPlurial')" />
                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                    <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_ReactInYourDiscussions')" />
                                                  </xsl:otherwise>
                                                </xsl:choose>
                                              </td>
                                            </tr>
                                          </table>

                                        </td>
                                      </tr>
                                    </xsl:if>

                                    <!-- Discussion Bloc -->
                                    <xsl:if test="$discussions-count > 0">
                                      <tr>
                                        <td width="25%" height="70" valign="middle" align="center">
                                          <a href="{$hoozin-form-url}/my.aspx">
                                            <img src="cid:[contentId:discussionlogo,TEMPLATE\images\EucouncilRed\Hoozin\Mailing\my-notif-email-discussion.png]" />
                                          </a>
                                        </td>
                                        <td width="75%">

                                          <table cellspacing="0" border="0" width="100%" style="font-family:'Tahoma', Tahoma;">
                                            <tr>
                                              <td valign="middle">
                                                <a href="{$hoozin-form-url}/my.aspx" style="font-family:'Tahoma', Tahoma;font-size:35px; color:{$link-color};text-decoration:none;">
                                                  <xsl:value-of select="$discussions-count"/>
                                                </a>
                                              </td>
                                              <td style="font-size:14px;color:{$text-color};" valign="middle">
                                                <xsl:choose>
                                                  <xsl:when test="$discussions-count > 1">
                                                    <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_DiscussWithYouPlurial')" />
                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                    <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_DiscussWithYou')" />
                                                  </xsl:otherwise>
                                                </xsl:choose>
                                              </td>
                                            </tr>
                                          </table>

                                        </td>
                                      </tr>
                                    </xsl:if>

                                    <!-- Post Bloc -->
                                    <xsl:if test="$posts-count > 0">
                                      <tr>
                                        <td width="25%" height="70" valign="middle" align="center">
                                          <img src="cid:[contentId:otherlogo,TEMPLATE\images\EucouncilRed\Hoozin\Mailing\my-notif-email-others.png]" />
                                        </td>
                                        <td width="75%">

                                          <table cellspacing="0" border="0" width="100%" style="font-family:'Tahoma', Tahoma;">
                                            <tr>
                                              <td valign="middle">
                                                <a href="{$hoozin-form-url}/my.aspx" style="font-family:'Tahoma', Tahoma; font-size:35px; color:{$link-color}; text-decoration:none;">
                                                  <xsl:value-of select="$posts-count"/>
                                                </a>
                                              </td>
                                              <td style="font-size:14px;color:{$text-color};" valign="middle">
                                                <xsl:choose>
                                                  <xsl:when test="$posts-count > 1">
                                                    <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_PostedMessagePlurial')" />
                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                    <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_PostedMessage')" />
                                                  </xsl:otherwise>
                                                </xsl:choose>
                                              </td>
                                            </tr>
                                          </table>

                                        </td>
                                      </tr>
                                    </xsl:if>

                                  </table>

                                </xsl:if>

                                <xsl:if test="count($notifications[not(CommunityID = preceding-sibling::Notification/CommunityID) and Source != 'My'])" >

                                  <table style="background-color:{$background-color};" border="0" cellspacing="0" cellpadding="5" width="100%">
                                    <tr>
                                      <td height="30" width="30" style="font-family:'Tahoma', Tahoma;">
                                        <img style="vertical-align:middle;" src="cid:[contentId:notificationcommunitylogo,TEMPLATE\images\EucouncilRed\Hoozin\Mailing\notif-community-icon.png]" />
                                      </td>
                                      <td height="30" style="font-family:'Tahoma', Tahoma;">
                                        <span style="color: {$text-color}; font-size: 13px; font-weight: bold; font-family:'Tahoma', Tahoma;">
                                          <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_Communities')" />
                                        </span>
                                      </td>
                                    </tr>
                                  </table>

                                  <!-- Community Bloc -->
                                  <xsl:for-each select="$notifications[not(CommunityID = preceding-sibling::Notification/CommunityID) and Source != 'My']">
                                    <xsl:variable name="communityID" select="CommunityID" />
                                    <xsl:variable name="community" select="$communities[WebGUID = $communityID]" />
                                    <xsl:variable name="communityUrl" select="$community/WebServerRelativeUrl" />
                                    <xsl:variable name="communityName" select="$community/Name" />

                                    <table cellpadding="5" cellspacing="0" border="0" width="550" style="margin-bottom:50px; border-top:1px solid {$background-light-color};">
                                      <tr>

                                        <!-- Community data -->
                                        <td width="150" valign="top">

                                          <table cellpadding="0" cellspacing="0" border="0" width="150" style="margin-top:8px;">
                                            <tr>
                                              <td align="center" height="137" valign="top">
                                                <a href="{$hoozin-form-url}{$communityUrl}/My.aspx">
                                                  <img src="cid:{$communityID}" alt="{$communityName}" style="border:0;" />
                                                </a>
                                              </td>
                                            </tr>
                                            <tr>
                                              <td align="center">
                                                <a style="font-family:'Tahoma', Tahoma; font-size:12px; color:{$link-color}; text-decoration:none; font-weight:bold;" href="{$hoozin-form-url}{$communityUrl}/My.aspx">
                                                  <xsl:value-of select="$communityName"/>
                                                  <br />
                                                  <br />
                                                </a>
                                              </td>
                                            </tr>
                                          </table>

                                        </td>

                                        <td valign="top">

                                          <table style="border-bottom:1px solid {$background-light-color}; font-family:'Tahoma', Tahoma;" cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                              <td width="25">
                                                <span style="font-family:'Tahoma', Tahoma; font-size:35px; color:{$link-color};">
                                                  <xsl:value-of select="count($notifications[CommunityID = $communityID])"/>
                                                </span>
                                              </td>
                                              <td>
                                                <span style="font-size:14px;color:{$text-color};">
                                                  <xsl:choose>
                                                    <xsl:when test="count($notifications[CommunityID = $communityID]) = 1">
                                                      <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_Notification')" />
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                      <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_Notifications')" />
                                                    </xsl:otherwise>
                                                  </xsl:choose>
                                                </span>
                                              </td>
                                            </tr>
                                          </table>

                                          <xsl:for-each select="$notifications[(PostID = '' or count(. | key('notif', PostID)[1]) = 1) and CommunityID = $communityID]">
                                            <xsl:variable name="currentPostID" select="PostID" />
                                            <xsl:variable name="current-post-is-discussion" select="DiscussionID != ''" />
                                            <xsl:variable name="current-post-ownersid" select="OwnerSid" />
                                            <xsl:variable name="current-post-authorsid" select="PostAuthorSid" />
                                            <xsl:variable name="current-post-is-your-post" select="$current-post-authorsid = $current-post-ownersid" />
                                            <xsl:variable name="current-post-extended-properties-dataset" select="pxs:GetExtendedPropertiesNodeSet(Post/Item/z:row/@ows_ExtendedProperties)/ExtendedDataSet" />
                                            <xsl:variable name="current-post-likes" select="$notifications[PostID = $currentPostID and ActionPluginID='619a2f5b-8038-48b1-88e7-2cd1c4c8e329']" />
                                            <xsl:variable name="current-post-comments"  select="$notifications[PostID = $currentPostID and ActionPluginID='6d917e2a-7744-4d70-9dae-d2bb2c203dcb']" />
                                            <xsl:variable name="current-post-likes-count" select="count($current-post-likes)" />
                                            <xsl:variable name="current-post-comments-count"  select="count($current-post-comments)" />
                                            <xsl:variable name="current-post-is-reaction" select="$current-post-likes-count &gt; 0 or $current-post-comments-count &gt; 0" />
                                            <xsl:variable name="current-post-author" select="$profiles[Sid = $current-post-authorsid]" />
                                            <xsl:variable name="current-post-html-tweet" select="Post/Item/z:row/@ows_HoozinTweet" />
                                            <xsl:variable name="current-post-raw-tweet" select="$current-post-extended-properties-dataset/ExtendedProperties/RawTweet" />
                                            <xsl:variable name="current-post-link-serverurl" select="Post/Item/z:row/@ows_ServerUrl" />
                                            <xsl:variable name="current-post-link-filename" select="Post/Item/z:row/@ows_Title" />
                                            <xsl:variable name="current-post-content-type-id" select="Post/Item/z:row/@ows_ContentTypeId" />
                                            <xsl:variable name="current-post-file-url" select="$current-post-extended-properties-dataset/PostExtendedProperties/FileUrl" />
                                            <xsl:variable name="current-post-file-name" select="$current-post-extended-properties-dataset/PostExtendedProperties/FileName" />
                                            <xsl:variable name="current-post-multiple-documents" select="$current-post-extended-properties-dataset/LinkedContent" />

                                            <xsl:variable name="current-post-tweet">
                                              <xsl:choose>
                                                <xsl:when test="$current-post-html-tweet != ''">
                                                  <xsl:value-of select="pxs:FormatHtmlTweetInNotificationEmail($current-post-html-tweet, $hoozin-form-url)"/>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                  <xsl:value-of select="pxs:TryEvaluateMacro($current-post-raw-tweet, false)"/>
                                                </xsl:otherwise>
                                              </xsl:choose>
                                            </xsl:variable>

                                            <table style="margin-top:7px;table-layout:fixed;" cellpadding="0" cellspacing="0" width="100%">

                                              <xsl:choose>

                                                <!-- REACTION -->
                                                <xsl:when test="$current-post-is-reaction">
                                                  <tr>
                                                    <td>
                                                      <xsl:choose>
                                                        <xsl:when test="$current-post-is-your-post">
                                                          <strong style="font-family:'Tahoma', Tahoma;font-weight:bold; font-size:12px; color:{$text-color}; line-height:120%;">
                                                            <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_YourMessageReactions')" />
                                                          </strong>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                          <strong style="font-family:'Tahoma', Tahoma;font-weight:bold; font-size:12px; color:{$text-color}; line-height:120%;">
                                                            <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_MessageFrom')" />
                                                          </strong>&#160;
                                                          <a style="font-family:'Tahoma', Tahoma; font-size:12px; line-height:120%; font-weight:bold;color:{$link-color}; text-decoration:none;" href="{$hoozin-form-url}/UserProfile.aspx?sid={$current-post-authorsid}">
                                                            <xsl:value-of select="$current-post-author/DisplayName" />
                                                          </a>&#160;<strong style="font-family:'Tahoma', Tahoma;font-weight:bold; font-size:12px; color:{$text-color}; line-height:120%;">
                                                            <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_Reactions')" />
                                                          </strong>
                                                        </xsl:otherwise>
                                                      </xsl:choose>
                                                    </td>
                                                  </tr>
                                                </xsl:when>

                                                <!-- DISCUSSION -->
                                                <xsl:when test="$current-post-is-discussion">
                                                  <tr>
                                                    <td>
                                                      <a style="font-family:'Tahoma', Tahoma; font-size:12px; line-height:17px; font-weight:bold;color:{$link-color}; text-decoration:none;" href="{$hoozin-form-url}/UserProfile.aspx?sid={$current-post-authorsid}">
                                                        <xsl:value-of select="$current-post-author/DisplayName" />
                                                      </a>
                                                      <strong style="font-family:'Tahoma', Tahoma; font-weight:bold; font-size:12px; color:{$text-color}; line-height:17px;">
                                                        <xsl:text>&#160;</xsl:text>
                                                        <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_CommunityDiscussWithYou')" />
                                                      </strong>
                                                    </td>
                                                  </tr>
                                                </xsl:when>

                                                <!-- POST -->
                                                <xsl:otherwise>
                                                  <tr>
                                                    <td>
                                                      <a style="font-family:'Tahoma', Tahoma; font-size:12px; line-height:120%; font-weight:bold; color:{$link-color}; text-decoration:none;" href="{$hoozin-form-url}/UserProfile.aspx?sid={$current-post-authorsid}">
                                                        <xsl:value-of select="$current-post-author/DisplayName" />
                                                      </a>
                                                      <strong style="font-family:'Tahoma', Tahoma;font-weight:bold; font-size:12px; color:{$text-color}; line-height:120%;">
                                                        <xsl:text>&#160;</xsl:text>
                                                        <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_CommunityPosted')" />
                                                      </strong>
                                                    </td>
                                                  </tr>
                                                </xsl:otherwise>

                                              </xsl:choose>

                                              <!-- Publication date -->
                                              <td>
                                                <span style="font-family:'Tahoma', Tahoma; font-size:11px; color:{$text-color}; line-height:15px;">
                                                  <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_PublishedOn')" />
                                                  <xsl:value-of select="pxs:GetLocalDate(Created, 'g')" />
                                                </span>
                                              </td>

                                              <!-- Tweet -->
                                              <tr>
                                                <td height="10" style="font-size:3px; line-height:3px;">&#160;</td>
                                              </tr>
                                              <tr>
                                                <td style="background-color:{$background-light-color}; padding:5px 10px 10px 10px; font-family:'Tahoma', Tahoma; font-size:12px; line-height:120%; word-wrap: break-word;">
                                                  <xsl:value-of select="$current-post-tweet" disable-output-escaping="yes"/>
                                                </td>
                                              </tr>

                                              <!-- File -->
                                              <xsl:if test="($current-post-link-serverurl and starts-with($current-post-content-type-id, '0x0101'))">
                                                <tr>
                                                  <td>

                                                    <table width="100%">
                                                      <tr>
                                                        <td width="7%"> </td>
                                                        <td width="93%" style="padding:5px 10px 5px 0; font-family:'Tahoma', Tahoma; font-size:11px; line-height:120%; border-bottom:1px solid {$background-light-color}; word-wrap: break-word;">

                                                          <xsl:choose>

                                                            <xsl:when test="count($current-post-multiple-documents) > 0">

                                                              <span style="color: {$text-color}; font-weight:bold;">
                                                                <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_DownloadMulitpleFiles')" />
                                                                <br />
                                                              </span>

                                                              <xsl:for-each select="$current-post-multiple-documents">

                                                                <xsl:variable name="document-title" select="./Title" />
                                                                <xsl:variable name="document-url" select="./FileUrl" />

                                                                <a style="font-family:'Tahoma', Tahoma; color:{$link-color}; font-size:11px; line-height:15px;" href="{$hoozin-form-url}/HoozinDownload.axd?fileurl={$document-url}" target="_blank">
                                                                  <span>
                                                                    <xsl:value-of select="$document-title"/>
                                                                  </span>
                                                                </a>
                                                                <br />

                                                              </xsl:for-each>

                                                            </xsl:when>

                                                            <xsl:otherwise>
                                                              <span style="color: {$text-color}; font-weight:bold;">
                                                                <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_DownloadFile')" />
                                                              </span>
                                                              <a style="font-family:'Tahoma', Tahoma; color:{$link-color}; font-size:11px; line-height:15px; font-weight:bold;" href="{$hoozin-form-url}/HoozinDownload.axd?fileurl={$current-post-link-serverurl}">
                                                                <xsl:value-of select="$current-post-link-filename" />
                                                              </a>
                                                            </xsl:otherwise>

                                                          </xsl:choose>

                                                        </td>
                                                      </tr>
                                                    </table>

                                                  </td>
                                                </tr>
                                              </xsl:if>


                                              <!-- Linked file -->
                                              <xsl:if test="$current-post-file-url">
                                                <tr>
                                                  <td>
                                                    <table width="100%">
                                                      <tr>
                                                        <td width="7%">&#160;</td>
                                                        <td width="93%" style="padding:5px 10px 5px 0; font-family:'Tahoma', Tahoma; font-size:11px; line-height:120%; border-bottom:1px solid {$background-light-color};">
                                                          <span style="color: {$text-color}; font-weight:bold;">
                                                            <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_DownloadFile')" />&#160;
                                                          </span>
                                                          <a style="font-family:'Tahoma', Tahoma; font-size:11px; line-height:15px; font-weight:bold; color:{$link-color}; text-decoration:none;">
                                                            <xsl:attribute name="href">
                                                              <xsl:choose>
                                                                <xsl:when test="not(starts-with($current-post-file-url, $communityUrl))">
                                                                  <xsl:value-of select="concat($hoozin-form-url, $communityUrl, '/', $current-post-file-url)" />
                                                                </xsl:when>
                                                                <xsl:otherwise>
                                                                  <xsl:value-of select="concat($hoozin-form-url, '/', $current-post-file-url)" />
                                                                </xsl:otherwise>
                                                              </xsl:choose>
                                                            </xsl:attribute>
                                                            <xsl:value-of select="$current-post-file-name" disable-output-escaping="yes" />
                                                          </a>
                                                        </td>
                                                      </tr>
                                                    </table>
                                                  </td>
                                                </tr>
                                              </xsl:if>


                                              <!-- Link to the post content -->
                                              <xsl:if test="$currentPostID">
                                                <tr>
                                                  <td>
                                                    <table width="100%">
                                                      <tr>
                                                        <td width="7%">&#160;</td>
                                                        <td width="93%" style="padding:5px 10px 5px 0; font-family:'Tahoma', Tahoma; font-size:11px; line-height:120%; border-bottom:1px solid {$background-light-color};">
                                                          <a style="font-weight:bold; color:{$link-color}; text-decoration:none;" href="{$hoozin-form-url}{$communityUrl}/PostView.aspx?contentZone=true&amp;PostID={$currentPostID}">
                                                            <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_ViewPost')" />
                                                          </a>
                                                        </td>
                                                      </tr>
                                                    </table>
                                                  </td>
                                                </tr>
                                              </xsl:if>

                                              <!-- Like -->
                                              <xsl:if test="$current-post-likes-count > 0">
                                                <tr>
                                                  <td>
                                                    <table width="100%">
                                                      <tr>
                                                        <td width="7%">&#160;</td>
                                                        <td width="93%" style="padding:5px 10px 5px 0; border-bottom:1px solid {$background-light-color};">
                                                          <table>
                                                            <tr>
                                                              <td width="22" valign="middle">
                                                                <img style="vertical-align:10px;" src="cid:[contentId:likeicon,TEMPLATE\images\EucouncilRed\Hoozin\Mailing\like-icon.png]" />
                                                              </td>
                                                              <td valign="middle">
                                                                <span style="font-family:'Tahoma', Tahoma; font-size:11px; line-height:120%; color: {$text-color}; font-weight:bold;">
                                                                  <xsl:value-of select="$current-post-likes-count"/>&#160;
                                                                  <xsl:choose>
                                                                    <xsl:when test="$current-post-likes-count = 1">
                                                                      <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_LikedMessage')" />
                                                                    </xsl:when>
                                                                    <xsl:otherwise>
                                                                      <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_LikedMessagePlurial')" />
                                                                    </xsl:otherwise>
                                                                  </xsl:choose>
                                                                </span>
                                                              </td>
                                                            </tr>
                                                          </table>
                                                        </td>
                                                      </tr>
                                                    </table>
                                                  </td>
                                                </tr>
                                              </xsl:if>

                                              <!-- comment -->
                                              <xsl:if test="$current-post-comments-count > 0">
                                                <tr>
                                                  <td>

                                                    <table width="100%" sttyle="table-layout: fixed;">
                                                      <tr>
                                                        <td width="7%">&#160;</td>
                                                        <td width="93%" style="padding:5px 10px 5px 0; border-bottom:1px solid {$background-light-color};">
                                                          <table>
                                                            <tr>
                                                              <td width="22" valign="middle">
                                                                <img src="cid:[contentId:commenticon,TEMPLATE\images\EucouncilRed\Hoozin\Mailing\comment-icon.png]" />
                                                              </td>
                                                              <td valign="middle">
                                                                <span style="font-family:'Tahoma', Tahoma; font-size:11px; line-height:120%; color: {$text-color}; font-weight:bold;">
                                                                  <xsl:value-of select="$current-post-comments-count"/>&#160;
                                                                  <xsl:choose>
                                                                    <xsl:when test="$current-post-comments-count = 1">
                                                                      <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_CommentedMessage')" />
                                                                    </xsl:when>
                                                                    <xsl:otherwise>
                                                                      <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_CommentedMessagePlurial')" />
                                                                    </xsl:otherwise>
                                                                  </xsl:choose>
                                                                </span>
                                                              </td>
                                                            </tr>
                                                          </table>
                                                        </td>
                                                      </tr>
                                                      <xsl:for-each select="$current-post-comments">
                                                        <xsl:variable name="current-comment-action-id" select="ActionID"/>
                                                        <xsl:variable name="current-comment-author-sid" select="AuthorSid" />
                                                        <xsl:variable name="current-comment-author" select="$profiles[Sid = $current-comment-author-sid]" />
                                                        <xsl:variable name="current-comment-date" select="Created" />
                                                        <xsl:variable name="current-comment-text" select="$current-post-extended-properties-dataset/Action[ID = $current-comment-action-id]/CommentText" />
                                                        <tr>
                                                          <td width="7%">&#160;</td>
                                                          <td width="93%" style="padding:5px 10px 3px 0; font-family:'Tahoma', Tahoma; font-size:11px; line-height:120%;">
                                                            <a style="font-family:'Tahoma', Tahoma; font-size:11px; line-height:15px; font-weight:bold; color:{$link-color}; text-decoration:none;" href="{$hoozin-form-url}/UserProfile.aspx?sid={$current-comment-author-sid}">
                                                              <xsl:value-of select="$current-comment-author/DisplayName" />
                                                            </a>
                                                            <span style="font-family:'Tahoma', Tahoma; font-size:11px; color:{$text-color}; line-height:120%;">
                                                              - <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-notifications_On')" /><xsl:value-of select="pxs:GetLocalDate(Created, 'g')" />
                                                            </span>
                                                          </td>
                                                        </tr>
                                                        <tr>
                                                          <td width="7%">&#160;</td>
                                                          <td width="93%" style="background-color:{$background-light-color}; padding:5px 10px 5px 10px; font-family:'Tahoma', Tahoma; font-size:11px; line-height:120%; word-wrap: break-word;">
                                                            <xsl:value-of select="$current-comment-text" disable-output-escaping="yes"/>
                                                          </td>
                                                        </tr>
                                                        <tr>
                                                          <td height="10" style="font-size:10px; line-height:10px;">&#160;</td>
                                                        </tr>
                                                      </xsl:for-each>
                                                    </table>
                                                  </td>
                                                </tr>
                                              </xsl:if>
                                              <tr>
                                                <td height="10" style="font-size:10px; line-height:10px;">&#160;</td>
                                              </tr>
                                            </table>
                                          </xsl:for-each>
                                        </td>
                                      </tr>
                                    </table>
                                    <br />
                                  </xsl:for-each>
                                </xsl:if>

                              </td>
                            </tr>
                          </table>

                          <table width="100%">
                            <tr>
                              <td height="20">

                              </td>
                            </tr>
                          </table>

                        </td>
                      </tr>
                    </table>

                  </td>
                </tr>
              </table>

            </td>
          </tr>

          <tr>
            <td height="50">

              <!--<xsl:call-template name="footer-email">
              </xsl:call-template>-->

            </td>
          </tr>

        </table>

      </body>
    </html>
  </xsl:template>

</xsl:stylesheet>
