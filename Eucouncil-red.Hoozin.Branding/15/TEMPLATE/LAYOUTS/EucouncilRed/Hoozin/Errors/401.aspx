﻿<%@ Assembly Name="Microsoft.SharePoint.ApplicationPages, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%>
<%@ Assembly Name="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c"%> 
<%@ Page Language="C#" Inherits="Microsoft.SharePoint.ApplicationPages.AccessDeniedPage" MasterPageFile="/_layouts/15/Prexens/hoozin/MasterPages/login.master" %> 
<%@ Import Namespace="Microsoft.SharePoint.WebControls" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Assembly Name="Prexens.Hoozin.Engine, Version=14.0.0.0, Culture=neutral, PublicKeyToken=0afb724dcb92ffcb" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<script runat="server">
	protected override void OnInit(EventArgs e)
	{
			this.Page.Response.Headers.Add("hoozinStatusCode", "401");
			base.OnInit(e);
	}
</script>

<asp:Content ID="Content1" ContentPlaceHolderId="PlaceHolderPageTitle" runat="server">
	<SharePoint:EncodedLiteral ID="EncodedLiteral3" runat="server" text="<%$PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_Hoozin401_PageTitle%>" EncodeMethod='HtmlEncode'/>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderId="PlaceHolderPageTitleInTitleArea" runat="server">

</asp:Content>
<asp:Content ID="Content3" contentplaceholderid="PlaceHolderPageImage" runat="server">

</asp:Content>
<asp:Content ID="Content4" contentplaceholderid="PlaceHolderAdditionalPageHead" runat="server">
	<meta name="Robots" content="NOINDEX " />
	<meta name="SharePointError" content="1" />
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
	<link href="/_layouts/15/EucouncilRed/Hoozin/LESS/hoozinBoxPage.less?v=1002" rel="stylesheet" type="text/css"  />

    <script type="text/javascript">
		var goBackOrGoHome = function () {
			if (typeof history === 'object' && history.length > 1) {
				history.back();
			}
			else {
				window.location.href = '<%= SPContext.Current.Site.Url %>';
			}
		}
	</script>

</asp:Content>

<asp:Content ContentPlaceHolderId="PlaceHolderMain" runat="server">
	<div id="boxScreenWrapper" class="errors privat">
		<!-- Zone For Informations or Picture -->
		<div id="infosZone">
			<span>401</span>
		</div>

		<!-- Principale Zone -->
		<div class="contentZone">

			<div id="hoozinLogo"></div>

			<h3>
				<SharePoint:EncodedLiteral ID="EncodedLiteral8" runat="server" Text="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_Hoozin401_PageHeaderText %>" />
			</h3>
			
			<p class="errorMessage">
				<SharePoint:EncodedLiteral ID="EncodedLiteral1" runat="server" Text="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_Hoozin401_ErrorMessagePart1 %>" />
				<a href="javascript:history.back()"><SharePoint:EncodedLiteral ID="EncodedLiteral2" runat="server" Text="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_Hoozin401_ErrorMessagePart2 %>" /></a>
			</p>
			<p class="errorCode">
				<SharePoint:EncodedLiteral ID="EncodedLiteral4" runat="server" Text="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_Hoozin401_ErrorCode %>" />
			</p>
			<asp:HyperLink id="HLinkRequestAccess" Text="<%$SPHtmlEncodedResources:wss,accessDenied_requestAccess%>" CssClass="ms-descriptiontext" runat="server"/>

			<asp:Panel id="PanelUserName" runat="server">
			<SharePoint:EncodedLiteral ID="EncodedLiteral5" runat="server" Text="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_Hoozin401_YourActuallyConnectedAs %>" />
			<br />
<asp:Label id="LabelUserName" runat="server"/> - 
			<asp:HyperLink id="HLinkLoginAsAnother" Text="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_Hoozin401_ChangeAccount %>" CssClass="ms-descriptiontext" runat="server"/>
			</asp:Panel>
			
			<!-- Return button -->
			<a href="javascript:goBackOrGoHome();" class="btn btn-primary"><asp:Label runat="server" Text="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_Hoozin401_Path %>"></asp:Label></a>
		</div>
	</div>


</asp:Content>
