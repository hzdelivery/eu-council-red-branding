﻿<%@ Assembly Name="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Assembly Name="Prexens.Hoozin.Engine, Version=14.0.0.0, Culture=neutral, PublicKeyToken=0afb724dcb92ffcb" %>

<%@ Page Language="C#"  Inherits="Microsoft.SharePoint.IdentityModel.Pages.IdentityModelSignInPageBase"  MasterPageFile="/_layouts/15/Prexens/hoozin/MasterPages/login.master" %>

<%@ Import Namespace="Microsoft.SharePoint.WebControls" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
	<SharePoint:EncodedLiteral runat="server" EncodeMethod="HtmlEncode" ID="ClaimsFormsPageTitle" Visible="false" />
	<SharePoint:EncodedLiteral ID="EncodedLiteral2" runat="server" Text="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_HoozinRecoverPassword_PageTitle %>" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
	<SharePoint:EncodedLiteral runat="server" EncodeMethod="HtmlEncode" ID="ClaimsFormsPageTitleInTitleArea" Visible="false" />
	<SharePoint:EncodedLiteral ID="EncodedLiteral3" runat="server" Text="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_HoozinRecoverPassword_PageTitle %>" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderSiteName" runat="server" />

<asp:Content ID="Content6" runat="server" ContentPlaceHolderID="PlaceHolderAdditionalPageHead">

	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
	<link href="/_layouts/15/EucouncilRed/Hoozin/LESS/hoozinBoxPage.less?v=1002" rel="stylesheet" type="text/css"  />

	<script src="/_layouts/15/Prexens/Hoozin/JS/jQuery/jquery-1.8.3.min.js" type="text/javascript"></script>
	<script src="/_layouts/15/Prexens/Hoozin/JS/HoozinUtils.js" type="text/javascript"></script>
	<script src="/_layouts/15/Prexens/Hoozin/JS/Hoozin.js" type="text/javascript"></script>
	<script src="/_layouts/15/Prexens/Hoozin/JS/HoozinFactory.js" type="text/javascript"></script>

	<script type="text/javascript">
		
		var email;
		
		$(function () {
			
			hoozin.Factory.set('<%= Microsoft.SharePoint.SPContext.Current.Site.ID.ToString() %>', '<%= Microsoft.SharePoint.SPContext.Current.Site.RootWeb.ID.ToString() %>', null);
			
			$("#emailInput")
			.off('keypress')
			.on('keypress', function (e) {
				if (e.which == 13) {
					e.preventDefault();
					launchResetPassword();
				}
			});

			$('.sendPasswordButton')
			.off('click')
			.on('click', function (e) {
				launchResetPassword();
			});

			var buttonLoadingMode = function () {
				$('.sendPasswordButton')
					.width(66)
					.height(17)
					.val('')
					.css('background-position', 'center center')
					.attr('disabled', 'disabled');
			}

			var buttonClickMode = function () {
				$('.sendPasswordButton')
					.width('auto')
					.height('auto')
					.val("<%= Prexens.Hoozin.Engine.ResourcesUtils.GetResource("EucouncilRed.Hoozin.Branding", "ASPX_HoozinRecoverPassword_Send") %>")
					.css('background-position', '-9999px center')
					.removeAttr('disabled');
			}

			var launchResetPassword = function () {
				
				buttonLoadingMode();

				email = $.trim($('#emailInput').val());

				if (email != '') {
					
					$('#formError').slideUp('fast');
					$('#formDone').slideUp('fast');
					$('#emailInput').val('');

					hoozin.Factory.userResetPassword (
						email, 
						function(result) {
							buttonClickMode();
							$('#formDone').slideDown('fast');

						}, 
						function(result) {
							$('#formError').text("<%= Prexens.Hoozin.Engine.ResourcesUtils.GetResource("EucouncilRed.Hoozin.Branding", "ASPX_HoozinRecoverPassword_EmailNotRecognized") %>");
							$('#formError').slideDown('fast');
							buttonClickMode();
						}
					);
					
				} else {

					$('#formError').text("<%= Prexens.Hoozin.Engine.ResourcesUtils.GetResource("EucouncilRed.Hoozin.Branding", "ASPX_HoozinRecoverPassword_PleaseEnterEmail") %>");
					
					$('#formError').slideUp('fast');
					$('#formDone').slideUp('fast');
					
					$('#formError').slideDown('fast');

					buttonClickMode();
				}

			}

		});
	</script>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="PlaceHolderMain" runat="server">
	<div id="boxScreenWrapper" class="recoverPass">

		<!-- Zone For Informations or Picture -->
		<div id="infosZone">
		</div>

		<!-- Principale Zone -->
		<div class="contentZone">

			<div id="hoozinLogo"></div>

			<!-- Preload the Loader image on the Button to be sure it's well displayed when the user click on the connexion button -->
			<img src="/_layouts/15/images/EucouncilRed/Hoozin/loaders/hoozinLoader-20-color.gif?v=1002" style="display:none;" />

			<div id="formDone">
				<SharePoint:EncodedLiteral ID="EncodedLiteral4" runat="server" Text="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_HoozinRecoverPassword_YourPasswordHasBeenSent %>" /><br/>
			</div>
			<div id="formError"></div>

			<fieldset class="hoozinFielset">
				<!-- Name Field -->
				<div class="inputBlock">
					<div class="labelWrapper">
						<label>
							<SharePoint:EncodedLiteral ID="EncodedLiteral1" runat="server" Text="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_HoozinRecoverPassword_YourEmail %>" />
						</label>
						</div>
						<div class="inputWrapper">
							<input type="text" class="ms-inputuserfield" name="email" id="emailInput" />
						</div>
					<div class="clear"></div>
				</div>
			</fieldset>


			<a href="/_layouts/15/EucouncilRed/hoozin/pages/login.aspx" class="backLoginForm">
				<SharePoint:EncodedLiteral ID="EncodedLiteral6" runat="server" Text="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_HoozinRecoverPassword_BackToLoginForm %>" />
			</a>

			<input id="Button1" type="button" runat="server" class=" btn btn-primary sendPasswordButton" value="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_HoozinRecoverPassword_Send %>" name="sendPasswordButton" />
		</div>
		
	</div>

</asp:Content>
