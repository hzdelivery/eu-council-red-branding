﻿<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:pxs="http://xml.prexens.com/" xmlns:rs="urn:schemas-microsoft-com:rowset" xmlns:z="#RowsetSchema">

  <xsl:output indent="no" method="html"/>

  <!-- Parameters -->
  <xsl:param name="hoozin-form-url" />

  <!-- Includes list -->
  <xsl:include href="/_layouts/Prexens/Hoozin/XSLT/includes/global.xslt" />
  <xsl:include href="/_layouts/Prexens/Hoozin/XSLT/includes/helpers.xslt" />
  <xsl:include href="/_layouts/EucouncilRed/Hoozin/XSLT/Mailing/includes.xslt" />

  <!-- Root of rendering logic -->
  <xsl:template match="/Hoozin">
    
    <xsl:variable name="admin" select="Admin/Profile" />
    <xsl:variable name="admin-sid" select="$admin/Sid" />
    <xsl:variable name="admin-display-name" select="$admin/DisplayName" />
    <xsl:variable name="admin-last-modified-short" select="$admin/LastModifiedShort" />
    <xsl:variable name="admin-profile" select="concat($hoozin-form-url, '/UserProfile.aspx?sid=', $admin-sid)" />
    <xsl:variable name="admin-avatar" select="concat('cid:', 'useravatar')" />
    
    <xsl:variable name="user" select="NewUser/Profile" />
    <xsl:variable name="user-password" select="NewUser/Password" />
    <xsl:variable name="user-sid" select="$user/Sid" />
    <xsl:variable name="user-display-name" select="$user/DisplayName" />
    <xsl:variable name="user-login" select="$user/Login" />
    
    <html>
      <head>
        <title>Email</title>

        <xsl:call-template name="css-email">
        </xsl:call-template>
	
      </head>

      <body>

        <table align="center" width="550" border="0" cellspacing="0" cellpadding="0" class="hz-email">
	        <tr>
		        <td bgcolor="{$background-color}" height="60">

              <xsl:call-template name="header-email">
                <xsl:with-param name="mail-type" select="'CreateUser'" />
              </xsl:call-template>
			        
		        </td>
	        </tr>
	        <tr>
		        <td bgcolor="{$background-color}">
		
			        <div class="hz-contentWrapper">
				        <div class="hz-content">
					        <table border="0" cellspacing="0" cellpadding="0" >
						        <tr>
							        <td width="65" rowspan="3" valign="top">
								        
												<table>
									        <tr>
														<td>
															<a href="{$admin-profile}">
																<img src="{$admin-avatar}" />
															</a>
														</td>
													</tr>
								        </table>

							        </td>
							        <td width="528">
								        <table class="hz-title">
									        <tr>
														<td>
															<a href="{$admin-profile}">
																<xsl:value-of select="$admin-display-name" />
															</a>
															<xsl:text>&#160;</xsl:text>
															<xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-create-user_OpenedAnAccount')" />
														</td>
													</tr>
								        </table>
							        </td>
						        </tr>
						        <tr>
							        <td width="528">

								        <table cellpadding="14" cellspacing="0" border="0" class="hz-message">
                          <tr>
                            <td>
															<div style="line-height:16px; margin-right:7px; margin-left:7px;">
																<xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-create-user_Hello')" />&#160;<xsl:value-of select="$user-display-name" />,<br /><br />
																<xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-create-user_WelcomeMessage')" /><br /><br />
																<xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-create-user_ConnectLink')" /><br />
																<a href="{$hoozin-form-url}"><xsl:value-of select="$hoozin-form-url" /></a><br /><br />
																<xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-create-user_UseTheseCredentials')" /><br />
																<xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-create-user_Login')" /><xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','Generic_Colon')" />&#160;<strong><xsl:value-of select="$user-login" /></strong><br />
																<xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-create-user_Password')" /><xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','Generic_Colon')" />&#160;<strong><xsl:value-of select="$user-password" /></strong><br /><br />
																<xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-create-user_OnceConnected')" /><br /><br />
																<xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-create-user_HoozinTeam')" /><br /><br />
																<xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-create-user_SeeYouSoon')" />
															</div>
                            </td>
                          </tr>
								        </table>

							        </td>
						        </tr>
						        <tr>
							        <td width="528" height="100" valign="middle">
												<table width="220" class="hz-gotoButton">
													<tr>
														<td>
															<a href="{$hoozin-form-url}">
																<xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-create-user_HoozinAccess')" />
															</a>
														</td>
													</tr>
												</table>
							        </td>
						        </tr>
					        </table>
				        </div><!--hz-content-->
			        </div><!--hz-contentWrapper-->
			
		        </td>
	        </tr>
	        <tr>
		        <td height="50">

              <!--<xsl:call-template name="footer-email">
              </xsl:call-template>-->

		        </td>
	        </tr>
	
        </table>

      </body>
    </html>

  </xsl:template>

</xsl:stylesheet>
