﻿<%@ Assembly Name="Prexens.Hoozin.Engine, Version=14.0.0.0, Culture=neutral, PublicKeyToken=0afb724dcb92ffcb" %>
<html xmlns:o="urn:schemas-microsoft-com:office:office" __expr-val-dir="ltr" lang="en-us" dir="ltr">
<head>
<title><asp:Literal runat="server" text="<%$PXSHiveResources:EucouncilRed.Hoozin.Branding,404_PageTitle%>"/></title>
<meta http-equiv="X-UA-Compatible" content="IE=Edge" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />

<link rel="shortcut icon" href="/_layouts/15/images/Prexens/Hoozin/logos/favicon.ico?v=1002" type="image/vnd.microsoft.icon" />
<link href="/_layouts/15/Prexens/Hoozin/LESS/hoozinBoxPage.less?v=1002&LoginPage=true" rel="stylesheet" type="text/css" />

</head>

<body>

<div id="s4-simple-card">
	<div id="s4-simple-card-content">
		<div id="s4-simple-content">
			<div id="s4-simple-error-content">
				<div id="boxScreenWrapper" class="errors lost">

					<!-- Zone For Informations or Picture -->
					<div id="infosZone">
						<span>404</span>
					</div>

					<!-- Principale Zone -->
					<div class="contentZone">

						<div id="hoozinLogo"></div>

						<p class="errorMessage">
							<asp:Literal runat="server" text="<%$PXSHiveResources:EucouncilRed.Hoozin.Branding,404_ErrorMessage%>"/>
						</p>
						
						<a href="/my.aspx" class="btn btn-primary"><asp:Literal runat="server" text="<%$PXSHiveResources:EucouncilRed.Hoozin.Branding,404_BackHome%>"/></a>
							
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>
    
</body>
</html>
