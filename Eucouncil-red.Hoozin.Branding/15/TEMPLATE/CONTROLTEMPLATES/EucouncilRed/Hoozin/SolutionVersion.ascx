﻿<%@ Assembly Name="Prexens.Hoozin.Engine, Version=14.0.0.0, Culture=neutral, PublicKeyToken=0afb724dcb92ffcb" %>
<%@ Control Language="C#" %>

<script type="text/javascript">
	hoozin.custom.versions.push(
		{
			'name': 'EucouncilRed.Hoozin.Branding',
			'version': 'v=1000',
			'issuer': 'YOUR_COMPANY_NAME',
			'image': '/_layouts/15/images/EucouncilRed/Hoozin/version/BrandingLogo.png', // PATH_TO_AN_IMAGE_218x218px (not mandatory, if not provided we use a default image)
			'description': '' // Not mandatory
		}
	);
</script>