﻿<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:pxs="http://xml.prexens.com/" xmlns:rs="urn:schemas-microsoft-com:rowset" xmlns:z="#RowsetSchema">

  <xsl:output indent="no" method="html"/>

  <!-- Parameters -->
  <xsl:param name="user-display-name" />
  <xsl:param name="user-sid" />
  <xsl:param name="community-url" />
  <xsl:param name="item-url"/>
	<xsl:param name="hoozin-form-url" />
	<xsl:param name="error"/>
	
  <!-- Includes list -->
  <xsl:include href="/_layouts/Prexens/Hoozin/XSLT/includes/global.xslt" />
  <xsl:include href="/_layouts/Prexens/Hoozin/XSLT/includes/helpers.xslt" />
  <xsl:include href="/_layouts/EucouncilRed/Hoozin/XSLT/Mailing/includes.xslt" />

  <!-- Root of rendering logic -->
  <xsl:template match="/Hoozin">
    
    <html>
      <head>
        <title>Email</title>

        <xsl:call-template name="css-email">
        </xsl:call-template>
	
      </head>

      <body>

        <table align="center" width="550" border="0" cellspacing="0" cellpadding="0" class="hz-email">
	        <tr>
		        <td bgcolor="{$background-color}" height="60">

              <xsl:call-template name="header-email">
                <xsl:with-param name="mail-type" select="'MultipleDownload'" />
              </xsl:call-template>
			        
		        </td>
	        </tr>
	        <tr>
		        <td bgcolor="{$background-color}">
		
			        <div class="hz-contentWrapper">
				        <div class="hz-content">
									
									<table border="0" cellspacing="0" cellpadding="0" >
						        <tr>
							        <td width="5" rowspan="3" valign="top">
								        <xsl:text>&#160;</xsl:text>
							        </td>
						        </tr>
										
										<xsl:choose>
											<xsl:when test="$error != ''">
												<tr>
													<td width="485">

														<table width="485" cellpadding="14" cellspacing="0" border="0" class="hz-message" >
															<tr>
																<td>
																	<div style="line-height:16px; margin-right:7px; margin-left:7px;">
																		<xsl:value-of select="$error" />
																	</div>
																</td>
															</tr>
														</table>

													</td>
												</tr>
											</xsl:when>
											
											<xsl:otherwise>
												<tr>
													<td width="485">

														<table width="485" cellpadding="14" cellspacing="0" border="0" class="hz-message" >
															<tr>
																<td>
																	<div style="line-height:16px; margin-right:7px; margin-left:7px;">
																		<xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-send-item-data_Hello')" />&#160;<xsl:value-of select="$user-display-name"/>,<br /><br />
																		<xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-download-multiple-files_DownloadMultipleFilesText')" />
																	</div>
																</td>
															</tr>
														</table>

													</td>
												</tr>
												<tr>
													<td width="425" align="right" height="100" valign="middle">
														
														<table width="220" class="hz-gotoButton">
															<tr>
																<td>
																	<a href="{concat($hoozin-form-url,$item-url)}">
																		<xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-download-multiple-files_DownloadArchive')" />
																		<br />
																	</a>
																</td>
															</tr>
														</table>
													
													</td>
												</tr>
											</xsl:otherwise>
											
										</xsl:choose>
										
					        </table>
									
								</div><!--hz-content-->
			        </div><!--hz-contentWrapper-->
			
		        </td>
	        </tr>
	        <tr>
		        <td height="50">

              <!--<xsl:call-template name="footer-email">
              </xsl:call-template>-->

		        </td>
	        </tr>
	
        </table>

      </body>
    </html>

  </xsl:template>

</xsl:stylesheet>
