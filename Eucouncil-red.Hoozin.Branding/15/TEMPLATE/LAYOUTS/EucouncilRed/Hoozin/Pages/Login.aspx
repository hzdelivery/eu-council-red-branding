﻿<%@ Assembly Name="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>

<%@ Page Language="C#" Inherits="Microsoft.SharePoint.IdentityModel.Pages.FormsSignInPage"	MasterPageFile="/_layouts/15/Prexens/hoozin/MasterPages/login.master" %>

<%@ Import Namespace="Microsoft.SharePoint.WebControls" %>
<%@ Assembly Name="Prexens.Hoozin.Engine, Version=14.0.0.0, Culture=neutral, PublicKeyToken=0afb724dcb92ffcb" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls"
	Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=14.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<asp:Content ID="Content1" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
	<SharePoint:EncodedLiteral runat="server" EncodeMethod="HtmlEncode" ID="ClaimsFormsPageTitle"
		Visible="false" />
	<SharePoint:EncodedLiteral ID="EncodedLiteral1" runat="server" Text="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_HoozinLogin_PageTitle %>" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PlaceHolderSiteName" runat="server" />
<asp:Content ID="Content2" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
	<SharePoint:EncodedLiteral runat="server" EncodeMethod="HtmlEncode" ID="ClaimsFormsPageTitleInTitleArea" Visible="false" />
	<SharePoint:EncodedLiteral ID="EncodedLiteral3" runat="server" Text="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_HoozinLogin_Title %>" />
</asp:Content>

<asp:Content ID="Content6" runat="server" ContentPlaceHolderID="PlaceHolderAdditionalPageHead">
	
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
	<meta name="hoozin:thumbnail" content="/_layouts/15/images/EucouncilRed/Hoozin/Dashboard/apple-touch-icon-144x144.png" />
	<meta name="hoozin:title" runat="server" content="Hoozin"  />
	<meta name="description" content="EucouncilRed digital workplace, social intranet and O365 help boost your workplace productivity and make your organization more agile with cost effective result"/>

	<link href="/_layouts/15/EucouncilRed/Hoozin/LESS/hoozinBoxPage.less?v=1002" rel="stylesheet" type="text/css"  />
	
	<script src="/_layouts/15/Prexens/Hoozin/js/jQuery/jquery-1.8.3.min.js" type="text/javascript"></script>

	<script type="text/javascript">
		
		$(function () {
			
			if (document.location.protocol != 'https:') {
				var SslWarning = document.getElementById('SslWarning');
				SslWarning.style.display = '';
			}

			$('#ctl00_PlaceHolderMain_signInControl_login')
			.off('click')
			.on('click', function (e) {

				$(this)
					.width(66)
					.height(17)
					.val('')
					.css('background-position', 'center center');

			});
		});
	</script>

</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="PlaceHolderMain" runat="server">
	<div id="boxScreenWrapper" class="login">

		<!-- Zone For Informations or Picture -->
		<div id="infosZone">
		</div>

		<!-- Principale Zone -->
		<div class="contentZone">

			<div id="hoozinLogo"></div>

			<!-- SubTitle -->
			<p>
				<SharePoint:EncodedLiteral runat="server" EncodeMethod="HtmlEncode" ID="EncodedLiteral5" Visible="false" />
				<SharePoint:EncodedLiteral ID="EncodedLiteral6" runat="server" Text="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_HoozinLogin_Title %>" />
			</p>

			<p id="SslWarning" style="display: none;">
				<SharePoint:EncodedLiteral runat="server" EncodeMethod="HtmlEncode" ID="ClaimsFormsPageMessage" />
			</p>
			
			
			<asp:Login ID="signInControl" FailureText="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_HoozinLogin_FailureText %>" runat="server" Width="100%">
				<LayoutTemplate>

					<!-- Error Message -->
					<asp:Label ID="FailureText" class="loginAlert" runat="server" />
					
					<fieldset class="hoozinFielset">
						<!-- Name Field -->
						<div class="inputBlock">
							<div class="labelWrapper">
								<label>
									<SharePoint:EncodedLiteral ID="EncodedLiteral1" runat="server" Text="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_HoozinLogin_UserName %>" EncodeMethod="HtmlEncode" />
								</label>
							</div>
							<div class="inputWrapper">
								<asp:TextBox ID="UserName" runat="server" class="ms-inputuserfield" />
							</div>
							<div class="clear"></div>
						</div>

						<!-- Password Field -->
						<div class="inputBlock">
							<div class="labelWrapper">
								<label>
									<SharePoint:EncodedLiteral ID="EncodedLiteral2" runat="server" Text="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_HoozinLogin_Password %>" EncodeMethod="HtmlEncode" />
								</label>
							</div>
							<div class="inputWrapper">
								<asp:TextBox ID="password" TextMode="Password" runat="server" class="ms-inputuserfield" />
							</div>
							<div class="clear"></div>
						</div>

						<!-- Forget Password link -->
						<p>
							<span class="loginHelper">
								<a id="forgotPassword" href="RecoverPassword.aspx">
									<SharePoint:EncodedLiteral ID="EncodedLiteral4" runat="server" Text="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_HoozinLogin_ForgotPassword %>" />
								</a>
							</span>
						</p>

						<!-- Checkbox Remember Me -->
						<div id="connexionAuto">
							<asp:CheckBox ID="RememberMe" Text="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_HoozinLogin_RememberMe %>" runat="server" />
						</div>

						<!-- Connexion Button -->
						<asp:Button ID="login" CommandName="Login" Text="<%$ PXSHiveResources:EucouncilRed.Hoozin.Branding,ASPX_HoozinLogin_ConnectButtonLabel %>" runat="server" CssClass="btn btn-primary"/>

					</fieldset>

				</LayoutTemplate>
			</asp:Login>
		</div>
		<div class="clear"></div>
	</div>
	
</asp:Content>
