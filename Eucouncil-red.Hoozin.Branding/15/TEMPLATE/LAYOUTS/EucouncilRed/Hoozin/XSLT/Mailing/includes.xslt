﻿<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:pxs="http://xml.prexens.com/" xmlns:rs="urn:schemas-microsoft-com:rowset" xmlns:z="#RowsetSchema">

  <xsl:variable name="text-color" select="'#6B6B6B'" />
  <xsl:variable name="footer-text-color" select="'#6B6B6B'" />
  <xsl:variable name="link-color" select="'#590c13'" />
  <xsl:variable name="border-top-color" select="'#EEEEEE'" />
  <xsl:variable name="button-text-color" select="'#FFFFFF'" />
  <xsl:variable name="background-color" select="'#FFFFFF'" />
  <xsl:variable name="background-light-color" select="'#E9E9E9'" />


  <xsl:template name="header-email">
    <xsl:param name="mail-type" />

    <xsl:variable name="mail-type-logo">
      <xsl:choose>
        <xsl:when test="$mail-type = 'WelcomeInCommunity'">community-logo.png</xsl:when>
        <xsl:when test="$mail-type = 'Notifications'">notification-logo.png</xsl:when>
        <xsl:otherwise>email-logo.png</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <table class="hz-header" width="550">
      <tr>
        <td width="250"  border="0" cellspacing="0" cellpadding="0">
          <a href="{$hoozin-form-url}">
            <img src="cid:[contentId:hoozinlogo,TEMPLATE\IMAGES\EucouncilRed\Hoozin\Mailing\logo-customer.png]" style="border:0;"/>
          </a>
        </td>
        <td width="255" align="right">
          <img src="cid:[contentId:emaillogo,TEMPLATE\IMAGES\EucouncilRed\Hoozin\Mailing\{$mail-type-logo}]" />
        </td>
      </tr>
    </table>

  </xsl:template>

  <!--<xsl:template name="footer-email">
    <div class="hz-bottom">
      <table style="margin-left:20px;" width="550">
        <tr>
          <td width="100"  border="0" cellspacing="0" cellpadding="0">
            <a href="http://www.hoozin.com">
              <img src="cid:[contentId:hoozinbottomlogo,TEMPLATE\IMAGES\EucouncilRed\Hoozin\Mailing\logo-hoozin-bottom.png]" style="border:0;"/>
            </a>
          </td>
          <td width="403" align="right">
            <span class="hz-bottom-wrapper">
              2405, route des Dolines - Drakkar D - BP 65 - 06902 Sophia-Antipolis - France<br />
              <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-reset-own-password_PhoneAbbr')" /> : +33 (0)9 77 19 55 81 - <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-reset-own-password_EmailAbbr')" /> : <a href="mailto:contact@hoozin.com">contact@hoozin.com</a>
            </span>
          </td>
        </tr>
      </table>
    </div>
  </xsl:template>-->

  <xsl:template name="css-email">
    <xsl:param name="mail-type" />

    <style type="text/css">

      .hz-email {margin: 30px auto 50px auto; padding: 0; font-size: 11px; color:<xsl:value-of select="$text-color"/>; }

      .hz-header {padding-bottom:14px; margin-left:20px;}

      .hz-contentWrapper { width:528px; border-top:1px solid <xsl:value-of select="$border-top-color"/>; padding-top:20px; }

      .hz-content { width:528px; color:<xsl:value-of select="$text-color"/>; font-family:'Tahoma', Tahoma; }
      .hz-content .hz-message { color:<xsl:value-of select="$text-color"/>; font-size:12px; font-family:'Tahoma', Tahoma; margin-bottom:15px; }
      .hz-content .hz-title { color:<xsl:value-of select="$text-color"/>; font-size:11px; font-weight: bold; font-family:'Tahoma', Tahoma; }

      .hz-content a:hover { color:<xsl:value-of select="$link-color"/>; text-decoration:underline; }
      .hz-content a:active { color:<xsl:value-of select="$link-color"/>; text-decoration:none; }
      .hz-content a:visited { color:<xsl:value-of select="$link-color"/>; text-decoration:none; }
      .hz-content a:link { color:<xsl:value-of select="$link-color"/>; text-decoration:none; }
      .hz-content a { color:<xsl:value-of select="$link-color"/>; text-decoration:none;}

      .hz-content .hz-gotoButton a:hover { color:<xsl:value-of select="$button-text-color"/>; text-decoration:underline; }
      .hz-content .hz-gotoButton a:active { color:<xsl:value-of select="$button-text-color"/>; text-decoration:underline; }
      .hz-content .hz-gotoButton a:visited { color:<xsl:value-of select="$button-text-color"/>; }
      .hz-content .hz-gotoButton a:link { color:<xsl:value-of select="$button-text-color"/>; }
      .hz-content .hz-gotoButton a { color:<xsl:value-of select="$button-text-color"/>; }
      .hz-content .hz-gotoButton { background-color:<xsl:value-of select="$link-color"/>; text-decoration:none; font-size:12px; text-align:center; font-family:'Tahoma', Tahoma; padding:10px; margin-left: 18px; }

      .hz-bottom { border-top:1px solid <xsl:value-of select="$border-top-color"/>; padding-top: 14px; }
      .hz-bottom-wrapper { font-family:'Tahoma', Tahoma; font-size:10px; color:<xsl:value-of select="$footer-text-color"/>; }
      .hz-bottom a { color:<xsl:value-of select="$link-color"/>; }
      .hz-bottom a:hover { color:<xsl:value-of select="$link-color"/>; text-decoration:underline; }
      .hz-bottom a:active { color:<xsl:value-of select="$link-color"/>; text-decoration:underline; }
      .hz-bottom a:visited { color:<xsl:value-of select="$link-color"/>; }

      <xsl:if test="$mail-type = 'Notifications'">

        .hz-link:hover { color:<xsl:value-of select="$link-color"/>; text-decoration:underline; font-family:'Tahoma', Tahoma; }
        .hz-link:active { color:<xsl:value-of select="$link-color"/>; text-decoration:none; font-family:'Tahoma', Tahoma;  }
        .hz-link:visited { color:<xsl:value-of select="$link-color"/>; text-decoration:none; font-family:'Tahoma', Tahoma; }
        .hz-link:link { color:<xsl:value-of select="$link-color"/>; text-decoration:none; font-family:'Tahoma', Tahoma; }
        .hz-link { color:<xsl:value-of select="$link-color"/>; text-decoration:none; font-family:'Tahoma', Tahoma; }

      </xsl:if>

    </style>

  </xsl:template>

</xsl:stylesheet>
