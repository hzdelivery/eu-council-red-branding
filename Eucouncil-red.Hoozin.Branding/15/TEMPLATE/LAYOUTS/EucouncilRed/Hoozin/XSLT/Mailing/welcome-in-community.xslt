﻿<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:pxs="http://xml.prexens.com/" xmlns:rs="urn:schemas-microsoft-com:rowset" xmlns:z="#RowsetSchema">

	<xsl:output indent="no" method="html"/>

	<!-- Parameters -->
	<xsl:param name="hoozin-form-url" />
	<xsl:param name="profile" />
	<xsl:param name="mail-custom-content" />

	<!-- Includes list -->
	<xsl:include href="/_layouts/Prexens/Hoozin/XSLT/includes/global.xslt" />
	<xsl:include href="/_layouts/Prexens/Hoozin/XSLT/includes/helpers.xslt" />
  <xsl:include href="/_layouts/EucouncilRed/Hoozin/XSLT/Mailing/includes.xslt" />

	<!-- Root of rendering logic -->
	<xsl:template match="/Hoozin">

		<xsl:variable name="community" select="Community" />
		<xsl:variable name="community-id" select="$community/WebGUID" />
		<xsl:variable name="community-name" select="$community/Name" />
		<xsl:variable name="community-description" select="$community/Description" />
		<xsl:variable name="community-absolute-url" select="concat($hoozin-form-url, $community/WebServerRelativeUrl)" />

		<xsl:variable name="admin-group" select="$community/Security/CommunityGroups/CommunityGroup[IsAdminsGroup='True']" />
		<xsl:variable name="admin-group-number" select="count($community/Security/CommunityGroups/CommunityGroup[IsAdminsGroup='True']/Profiles/Profile)" />
		<xsl:variable name="member-group-number" select="count($community/Security/CommunityGroups/CommunityGroup[IsMembersGroup='True']/Profiles/Profile)" />
		<xsl:variable name="visitor-group-number" select="count($community/Security/CommunityGroups/CommunityGroup[True='True']/Profiles/Profile)" />

		<xsl:variable name="apps" select="$community/AppsInstalled" />
		<xsl:variable name="stats" select="$community/Stats" />

		<html>
			<head>
				<title>Email</title>

        <xsl:call-template name="css-email">
        </xsl:call-template>

			</head>

			<body>
				<table align="center" width="550" border="0" cellspacing="0" cellpadding="0" class="hz-email">
					<tr>
						<td bgcolor="{$background-color}" height="60">

              <xsl:call-template name="header-email">
                <xsl:with-param name="mail-type" select="'WelcomeInCommunity'" />
              </xsl:call-template>
             
		        </td>
					</tr>
					<tr>
						<td bgcolor="{$background-color}" align="center">

							<div class="hz-contentWrapper">
								<div class="hz-content">
									<table border="0" cellspacing="0" cellpadding="0" >
										<tr>
											<td width="5" rowspan="3" valign="top">
												<xsl:text>&#160;</xsl:text>
											</td>
										</tr>
										<tr>
											<td width="485">

												<!-- Community avatar, name, desc and background -->
												<table height="93" width="485">
													<tr>
														<td>
															<img src="cid:communityBackground-{$community-id}" width="485" height="93" />
															<table width="465">
																<tr>
																	<td width="10" valign="top">
																		<xsl:text>&#160;</xsl:text>
																	</td>
																	<td width="90" valign="top">
																		<a href="{$community-absolute-url}" target="_blank">
																			<img src="cid:communityAvatar-{$community-id}" width="90" height="90" style="border:3px solid #FFF;"  border="0" />
																		</a>
																	</td>
																	<td width="10" valign="top">
																		<xsl:text>&#160;</xsl:text>
																	</td>
																	<td valign="top">
																		<div style="font-size:30px; margin:14px 0 10px 0; line-height: 30px; font-family:'Tahoma', Tahoma;">
																			<a href="{$community-absolute-url}" target="_blank">
																				<xsl:value-of select="$community-name" disable-output-escaping="yes"/>
																			</a>
																		</div>
																		<div style="font-family:'Tahoma', Tahoma; font-size: 11px; color: {$text-color};">
																			<xsl:value-of select="$community-description" disable-output-escaping="yes"/>
																		</div>
																	</td>
																</tr>
															</table>

														</td>
													</tr>
												</table>

												<!-- Mail content -->
												<table cellpadding="14" cellspacing="0" border="0" class="hz-message" width="480">
													<tr>
														<td>
															<div style="font-size: 12px; line-height: 18px; margin-left: 7px; margin-right: 7px; font-family:'Tahoma', Tahoma;">

                                <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-welcome-in-community-hello')" /><xsl:text>&#160;</xsl:text><xsl:value-of select="$profile/DisplayName"/>, <xsl:text>&#160;</xsl:text>
                                <br />
                                <br />

                                <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-welcome-in-community-content')" />
                                <br />
                                <br />

                                <xsl:if test="$mail-custom-content != ''">
                                  <xsl:value-of select="$mail-custom-content" disable-output-escaping="yes"/>
                                  <br />
                                  <br />
                                </xsl:if>

                                <a href="{$community-absolute-url}" target="_blank">
                                  <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-welcome-in-community-activity-stream')" />
                                </a>

                                <br />
                                <br />
                                <xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','mailing-welcome-in-community_KindRegards')" />
                                <xsl:text>&#160;</xsl:text>

															</div>
														</td>
													</tr>
												</table>

												<!-- Admins list and available apps -->
												<table cellpadding="14" cellspacing="0" border="0" class="hz-message" width="480">
													<tr>
														<td width="50%" valign="top">
															<div style="font-size: 13px; font-weight: bold; margin-left:7px; margin-bottom:7px; font-family:'Tahoma', Tahoma;">
																<xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','welcomeInCommunity_ManagersList')" />
															</div>

															<table>
																<!-- START LOOP -->
																<xsl:for-each select="$admin-group/Profiles/Profile">
																<tr>
																	<td width="35" valign="middle">
																		<div style="vertical-align: middle; margin-left:7px;">
																			<img src="cid:userAvatar-{Sid}" />
																		</div>
																	</td>
																	<td valign="middle">
																		<div style="font-family:'Tahoma', Tahoma; font-size: 12px; color: {$text-color};">
																			<xsl:value-of select="DisplayName" disable-output-escaping="yes"/>
																		</div>
																	</td>
																</tr>
																</xsl:for-each>
																<!-- END LOOP -->
															</table>

														</td>
														<td  width="50%" valign="top">
															<div style="font-size: 13px; font-weight: bold; margin-left:0px; margin-bottom:7px; font-family:'Tahoma', Tahoma;">
																<xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','welcomeInCommunity_AvailableApps')" />
															</div>

															<table>
																<xsl:choose>
																	<!-- No available apps in community -->
																	<xsl:when test="count($apps/App) = 0">
																		<tr>
																			<td valign="middle">
																				<div style="font-family:'Tahoma', Tahoma; font-size: 12px; color: {$text-color};">
																					<xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','welcomeInCommunity_NoAvailableApps')" />
																				</div>
																			</td>
																		</tr>
																	</xsl:when>
																	<xsl:otherwise>
																		<!-- START LOOP -->
																		<xsl:for-each select="$apps/App">
																		<tr>
																			<td valign="middle">
																				<div style="font-family:'Tahoma', Tahoma; font-size: 12px; color: {$text-color};">
																					<xsl:value-of select="pxs:TryEvaluateResource(DisplayName)" disable-output-escaping="yes"/>
																				</div>
																			</td>
																		</tr>
																		</xsl:for-each>
																		<!-- END LOOP -->
																	</xsl:otherwise>
																</xsl:choose>
															</table>

														</td>
													</tr>

												</table>

												<!-- Users stats and post count -->
												<table cellpadding="14" cellspacing="0" border="0" class="hz-message" width="480">
													<tr>
														<td width="50%" valign="top">
															<div style="font-size: 13px; font-weight: bold; margin-left:7px; margin-bottom:7px; font-family:'Tahoma', Tahoma;">
																<xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','welcomeInCommunity_CommunityStats')" />
															</div>

															<table>
																<tr>
																	<td width="35" valign="middle">
																		<div style="vertical-align: middle; margin-left:7px;">
																			<img src="cid:[contentId:hoozinStatsAdmin,TEMPLATE\IMAGES\EucouncilRed\Hoozin\Mailing\email-stats-admin.png]" />
																		</div>
																	</td>
																	<td valign="middle">
																		<div style="font-family:'Tahoma', Tahoma; font-size: 12px; color: {$text-color};">
																			<xsl:choose>
																				<xsl:when test="$admin-group-number &lt; 2">
																					<xsl:value-of select="$admin-group-number"/><xsl:text>&#160;</xsl:text><xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','welcomeInCommunity_Manager')" />
																				</xsl:when>
																				<xsl:otherwise>
																					<xsl:value-of select="$admin-group-number"/><xsl:text>&#160;</xsl:text><xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','welcomeInCommunity_Managers')" />
																				</xsl:otherwise>
																			</xsl:choose>
																		</div>
																	</td>
																</tr>
																<tr>
																	<td width="35" valign="middle">
																		<div style="vertical-align: middle; margin-left:7px;">
																			<img src="cid:[contentId:hoozinStatsMembers,TEMPLATE\IMAGES\EucouncilRed\Hoozin\Mailing\email-stats-membres.png]" />
																		</div>
																	</td>
																	<td valign="middle">
																		<div style="font-family:'Tahoma', Tahoma; font-size: 12px; color: {$text-color};">
																			<xsl:choose>
																				<xsl:when test="$member-group-number &lt; 2">
																					<xsl:value-of select="$member-group-number"/><xsl:text>&#160;</xsl:text><xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','welcomeInCommunity_Member')" />
																				</xsl:when>
																				<xsl:otherwise>
																					<xsl:value-of select="$member-group-number"/><xsl:text>&#160;</xsl:text><xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','welcomeInCommunity_Members')" />
																				</xsl:otherwise>
																			</xsl:choose>
																		</div>
																	</td>
																</tr>
																<tr>
																	<td width="35" valign="middle">
																		<div style="vertical-align: middle; margin-left:7px;">
																			<img src="cid:[contentId:hoozinStatsVisitors,TEMPLATE\IMAGES\EucouncilRed\Hoozin\Mailing\email-stats-visiteurs.png]" />
																		</div>
																	</td>
																	<td valign="middle">
																		<div style="font-family:'Tahoma', Tahoma; font-size: 12px; color: {$text-color};">
																			<xsl:choose>
																				<xsl:when test="$visitor-group-number &lt; 2">
																					<xsl:value-of select="$visitor-group-number"/><xsl:text>&#160;</xsl:text><xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','welcomeInCommunity_Visitor')" />
																				</xsl:when>
																				<xsl:otherwise>
																					<xsl:value-of select="$visitor-group-number"/><xsl:text>&#160;</xsl:text><xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','welcomeInCommunity_Visitors')" />
																				</xsl:otherwise>
																			</xsl:choose>
																		</div>
																	</td>
																</tr>
															</table>
														</td>
														<td  width="50%" valign="top">

															<table width="100%">
																<tr>
																	<td width="35" valign="middle">
																		<div style="text-align: center;">
																			<img src="cid:[contentId:hoozinStatsPost,TEMPLATE\IMAGES\EucouncilRed\Hoozin\Mailing\email-stats-post-icon.png]" />
																		</div>
																	</td>
																</tr>
																<tr>
																	<td valign="middle">
																		<div style="font-family:'Tahoma', Tahoma; text-align: center; font-size: 37px; color: {$text-color};">
																				<xsl:choose>
																				<xsl:when test="$stats/PostsCount &lt; 2">
																					<xsl:value-of select="$stats/PostsCount"/><xsl:text>&#160;</xsl:text><xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','welcomeInCommunity_Post')" />
																				</xsl:when>
																				<xsl:otherwise>
																					<xsl:value-of select="$stats/PostsCount" /><xsl:text>&#160;</xsl:text><xsl:value-of select="pxs:GetResource('Prexens.Hoozin.SharePoint','welcomeInCommunity_Posts')" />
																				</xsl:otherwise>
																			</xsl:choose>
																		</div>
																	</td>
																</tr>
															</table>
														</td>
													</tr>
												</table>

											</td>
										</tr>

									</table>
								</div>
								<!--hz-content-->
							</div>
							<!--hz-contentWrapper-->

						</td>
					</tr>

					<tr>
						<td height="50">

              <!--<xsl:call-template name="footer-email">
              </xsl:call-template>-->

		        </td>
					</tr>
				</table>

			</body>
		</html>

	</xsl:template>

</xsl:stylesheet>
