﻿<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:rs="urn:schemas-microsoft-com:rowset" xmlns:atom="http://www.w3.org/2005/Atom" xmlns:z="#RowsetSchema" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:pxs="http://xml.prexens.com/">

  <xsl:output indent="no" method="html"/>

  <!-- Parameters -->
  <xsl:param name="hide-title" select="false()" />
  <xsl:param name="hide-subtitle" select="false()" />
  <xsl:param name="hide-summary" select="false()" />

  <!-- Includes list -->
  <xsl:include href="/_layouts/15/Prexens/Hoozin/Authoring/XSLT/Includes/wcm/authoring.xslt" />

  <!-- Authoring client scripts -->
  <xsl:template name="pxs-wcm-create-script" >

    _pxsWCMPublishingPage_<xsl:value-of select="$webpart_id"/>.setOption(
			'resourcesBrowserUrl',
			'HoozinData.axd?settings=hoozin.apps.DocumentsManagement&amp;name=AppsDocumentsManagementResourcesBrowser'
    );

    _pxsWCMPublishingPage_<xsl:value-of select="$webpart_id" />.setOption(
			'pageMetadataUrl',
			'/_layouts/15/Prexens/Hoozin/Authoring/plugins/wcm/dialogs/page-metadata.aspx'
    );

  </xsl:template>

  <xsl:template name="pxs-wcm-init-script" />

  <!-- Page layout -->
  <xsl:template name="pxs-wcm-page-layout">

    <xsl:param name="is-in-edit-mode"/>

		<xsl:if test="$isDlg != '1'">
      <script type="text/javascript">

        $(function() {
          hoozin.Interface.withoutContext();

					//Deactive current publication binding
          var functionPublish =  $('.pubMajor').attr('onclick');
				  $('.pubMajor').attr('onclick', '');
          
          $('#contentZone')
          .off('click', '.pxs-wcm-page-action.pubMajor')
          .on('click', '.pxs-wcm-page-action.pubMajor', function(){
            
            if(!hasValue(_pxsWCMPublishingPage_<xsl:value-of select="$webpart_id"/>.getPageSettings().HoozinPublicationStartDate))
            {
              var todayDate = $.datepicker.formatDate('yy-mm-dd', new Date())
              
              $('#pxs-wcm-page-hoozinpublicationstartdate').text(todayDate);
              _pxsWCMPublishingPage_<xsl:value-of select="$webpart_id"/>.propertyHasChanges('pxs-wcm-page-hoozinpublicationstartdate');     
            }
            
            // Execute publish method
            eval(functionPublish);
          });

        });

      </script>
    </xsl:if>

    <xsl:variable name="pxs-wcm-page-contributor" select="atom:contributor/atom:name" />
    <xsl:variable name="pxs-wcm-page-updated" select="atom:updated" />
    <xsl:variable name="pxs-wcm-page-summary" select="pxs:GetAtomField(., 'PageSummary')" />
    <xsl:variable name="pxs-wcm-page-show-toc" select="pxs:GetAtomField(., 'ShowToc')" />
    <xsl:variable name="pxs-wcm-page-layout-id" select="pxs:GetAtomField(., 'PageLayout')" />
    <xsl:variable name="pxs-wcm-page-news-image" select="pxs:GetAtomField(., 'WCMNewsImage')" />
    <xsl:variable name="pxs-show-languages" select="pxs:GetAtomField(., 'HoozinDefaultLanguage') != ''" />


    <xsl:choose>
      <xsl:when test="$is-in-edit-mode = 'Edit'">
        <xsl:call-template name="pxs-wcm-page-authoring-menu" >
          <xsl:with-param name="show-metadata" select="true()" />
          <xsl:with-param name="show-languages" select="$pxs-show-languages" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:if test="$pxs-show-languages and $isDlg != '1'">
          <xsl:call-template name="pxs-wcm-page-switch-language-menu" >
          </xsl:call-template>
        </xsl:if>
      </xsl:otherwise>
    </xsl:choose>


    <a name="top" />

    <!-- TITLE -->
    <xsl:if test="not($hide-title) and ($pxs-wcm-editing-enabled or pxs:GetAtomField(., 'PageTitle') != '')">
      <h2>
        <xsl:call-template name="pxs-wcm-layout-field">
          <xsl:with-param name="field-id" select="'PageTitle'" />
        </xsl:call-template>
      </h2>
    </xsl:if>

    <!-- TOC -->
    <xsl:if test="$pxs-wcm-page-show-toc = 'on'">
      <xsl:call-template name="pxs-wcm-layouts-toc">
        <xsl:with-param name="pxs-wcm-page-show-toc" select="$pxs-wcm-page-show-toc" />
        <xsl:with-param name="pxs-wcm-page-layout-id" select="$pxs-wcm-page-layout-id" />
      </xsl:call-template>
    </xsl:if>

    <!-- CHAPO -->
    <xsl:if test="not($hide-summary) and ($pxs-wcm-editing-enabled or $pxs-wcm-page-summary)">
      <div id="chapo">
        <xsl:call-template name="pxs-wcm-layout-field">
          <xsl:with-param name="field-id" select="'PageSummary'" />
          <xsl:with-param name="field-value" select="$pxs-wcm-page-summary" />
          <xsl:with-param name="is-html" select="true()" />
        </xsl:call-template>
      </div>
    </xsl:if>

    <xsl:if test="not($pxs-wcm-editing-enabled) and $pxs-wcm-page-news-image != ''">
      <img src="{$pxs-wcm-page-news-image}" />
    </xsl:if>

    <xsl:if test="$pxs-wcm-editing-enabled">
      
      <!-- hidden but real fields -->
      <div style="display:none">
        <xsl:call-template name="pxs-wcm-layout-field">
          <xsl:with-param name="field-id" select="'HoozinPublicationStartDate'" />
        </xsl:call-template>
      </div>


      <!-- image -->
      <div class="inputBlock" style="background-color:#f9f9f9; padding:13px; margin-bottom:13px;">
        <div class="labelWrapper">
          <label style="color:#aaa; font-size:11px;">
						<xsl:value-of select="pxs:GetResource('EucouncilRed.Hoozin.Branding','AuthoringTemplate_Headlines_Image')" />
          </label>
          
        </div>
        <div class="inputWrapper">
          <div>
            <xsl:call-template name="pxs-wcm-layout-picture">
              <xsl:with-param name="field-id" select="'WCMNewsImage'" />
              <xsl:with-param name="field-value" select="$pxs-wcm-page-news-image" />
            </xsl:call-template>
          </div>
        </div>
        <div class="clear"></div>
      </div>
      
      

    </xsl:if>


    <!-- LAYOUT  -->

    <div class="full-width-wrapper full-width-wrapper-top">
      <div class="full-width aZone">
        <xsl:call-template name="pxs-wcm-layout-zone">
          <xsl:with-param name="zone-id" select="'Main'" />
          <xsl:with-param name="sections" select="$pxs-wcm-page-sections[Zone='Main']" />
          <xsl:with-param name="adjacent-margin-right" select="'35px'" />
        </xsl:call-template>
      </div>
    </div>

    <xsl:choose>

      <xsl:when test="$pxs-wcm-page-layout-id = 'two-columns-50'">

        <div class="two-columns-50-wrapper">

          <div class="two-columns-50 aZone">
            <xsl:call-template name="pxs-wcm-layout-zone">
              <xsl:with-param name="zone-id" select="'Left'" />
              <xsl:with-param name="sections" select="$pxs-wcm-page-sections[Zone='Left']" />
            </xsl:call-template>
          </div>

          <div class="two-columns-50-sep"></div>

          <div class="two-columns-50 aZone">
            <xsl:call-template name="pxs-wcm-layout-zone">
              <xsl:with-param name="zone-id" select="'Right'" />
              <xsl:with-param name="sections" select="$pxs-wcm-page-sections[Zone='Right']" />
            </xsl:call-template>
          </div>

        </div>

        <!--<div class="clear"></div>-->
      </xsl:when>

      <xsl:when test="$pxs-wcm-page-layout-id = 'two-columns-70'">

        <div class="two-columns-70-wrapper">

          <div class="two-columns-70 two-columns-70-left aZone">
            <xsl:call-template name="pxs-wcm-layout-zone">
              <xsl:with-param name="zone-id" select="'Left'" />
              <xsl:with-param name="sections" select="$pxs-wcm-page-sections[Zone='Left']" />
            </xsl:call-template>
          </div>

          <div class="two-columns-70-sep"></div>

          <div class="two-columns-70 two-columns-70-right aZone">
            <xsl:call-template name="pxs-wcm-layout-zone">
              <xsl:with-param name="zone-id" select="'Right'" />
              <xsl:with-param name="sections" select="$pxs-wcm-page-sections[Zone='Right']" />
            </xsl:call-template>
          </div>

        </div>

      </xsl:when>

    </xsl:choose>

    <xsl:if test="$pxs-wcm-page-layout-id = 'two-columns-50' or $pxs-wcm-page-layout-id = 'two-columns-70'">
      <div class="full-width-wrapper">
        <div class="full-width aZone">
          <xsl:call-template name="pxs-wcm-layout-zone">
            <xsl:with-param name="zone-id" select="'Footer'" />
            <xsl:with-param name="sections" select="$pxs-wcm-page-sections[Zone='Footer']" />
            <xsl:with-param name="adjacent-margin-right" select="'35px'" />
          </xsl:call-template>
        </div>
      </div>
    </xsl:if>

  </xsl:template>

  <!-- Section layout -->
  <xsl:template name="pxs-wcm-section-layout">
    <xsl:param name="zone-id" />

    <xsl:if test="$pxs-wcm-editing-enabled or Title != ''">
      <h3>
        <xsl:call-template name="pxs-wcm-render-section-title" />
      </h3>
    </xsl:if>

    <div>
      <xsl:if test="Height and Height != ''">
        <xsl:attribute name="style">
          <xsl:text>overflow:auto;</xsl:text>
          <xsl:value-of select="concat('height:', Height, ';')"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:call-template name="pxs-wcm-render-section-content" />
    </div>

  </xsl:template>

</xsl:stylesheet>
