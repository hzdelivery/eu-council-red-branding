﻿<%@ Assembly Name="Prexens.Hoozin.Engine, Version=14.0.0.0, Culture=neutral, PublicKeyToken=0afb724dcb92ffcb" %>
<%@ Control Language="C#" %>

<script type="text/javascript">
	
	var _searchMenuIsOpen = false;
	var _animSpeed = 250;

	/* INIT MY MENY FOR HIGH RES */
	var _initSearchMenu = function () {

		$('#switchSearchBt')
		.off('click')
		.on('click', function (e) {
			//e.preventDefault();

			if (!_searchMenuIsOpen) {

				_searchMenuIsOpen = true;

				$('#switchSearchWindow').fadeIn(_animSpeed, function () {

					// When the MyMenu loose focus on a click
					$('html')
					.off('click.closePopHoverWindows')
					.on('click.closePopHoverWindows', function (e) {
						var doNotCloseOnElement = $('#switchSearchWindow');
						if (!$(e.target).is(doNotCloseOnElement) && !$.contains(document.getElementById('myWindowWrapper'), e.target)) {
							_closeSearchMenu();
						}
					});

				});
			}

		});
	}

	var _closeSearchMenu = function () {
		$('html').off('click.closePopHoverWindows');
		$('#switchSearchWindow').hide();
		_searchMenuIsOpen = false;
	}

	var _initSwitchSearch = function() {
		$('#header')
		.off('click','#switchSearchWindow .chooseSearch:not(current)')
		.on('click', '#switchSearchWindow .chooseSearch:not(current)',function (e) {
			
			$('#switchSearchWindow .chooseSearch').removeClass('current');
			$(this).addClass('current');

			var imgUrl = $(this).attr('src');
			var dataSearch =  $(this).attr('data-search');

			$('#switchSearchBt .currentSearch')
				.attr('src',imgUrl)
				.attr('data-search',dataSearch);

			$('#searchForm').attr('data-search',dataSearch);
			$('.searchInput').focus();


		});

	}

</script>



<div id="searchFormWrapper">
	<div id="searchForm" data-search="hoozin">
		<input type="text" value="" autocomplete="off" autocorrect="off" class="searchInput" runat="server" placeholder="<%$PXSHiveResources:Prexens.Hoozin.SharePoint, Header_SearchFormPlaceholder%>" />
		<input type="button" value=" " id="searchButton" />        
	</div>
</div>
